// INCLUDE JQUERY & JQUERY UI 1.12.1
$(function () {
  $('#datepicker').datepicker({
    changeMonth: true,
    changeYear: true,
    showOtherMonths: true,
    selectOtherMonths: true,
    dateFormat: 'dd-mm-yy', duration: 'fast'
  })
})



//jQuery time
var current_fs, next_fs, previous_fs //fieldsets
var left, opacity, scale //fieldset properties which we will animate
var animating //flag to prevent quick multi-click glitches

$('.next').click(function () {
  // debugger;
  if (!validateStep(1)) {
    return;
    // return document.querySelector('#general-error').scrollIntoView({ behavior: "smooth" });

  };
  // alert('next!')
  if (animating) return false
  animating = true

  current_fs = $(this).parent()
  next_fs = $(this).parent().next()

  //activate next step on progressbar using the index of next_fs
  $('#progressbar li').eq($('fieldset').index(next_fs)).addClass('active')

  //show the next fieldset
  next_fs.show()
  //hide the current fieldset with style
  current_fs.animate({ opacity: 0 }, {
    step: function (now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale current_fs down to 80%
      scale = 1 - (1 - now) * 0.2
      //2. bring next_fs from the right(50%)
      left = (now * 50) + '%'
      //3. increase opacity of next_fs to 1 as it moves in
      opacity = 1 - now
      current_fs.css({ 'transform': 'scale(' + scale + ')' })
      next_fs.css({ 'left': left, 'opacity': opacity })
    },
    duration: 800,
    complete: function () {
      current_fs.hide()
      animating = false
    },
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  })
})

$('.previous').click(function () {
  if (animating) return false
  animating = true

  current_fs = $(this).parent()
  previous_fs = $(this).parent().prev()

  //de-activate current step on progressbar
  $('#progressbar li').eq($('fieldset').index(current_fs)).removeClass('active')

  //show the previous fieldset
  previous_fs.show()
  //hide the current fieldset with style
  current_fs.animate({ opacity: 0 }, {
    step: function (now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale previous_fs from 80% to 100%
      scale = 0.8 + (1 - now) * 0.2
      //2. take current_fs to the right(50%) - from 0%
      left = ((1 - now) * 50) + '%'
      //3. increase opacity of previous_fs to 1 as it moves in
      opacity = 1 - now
      current_fs.css({ 'left': left })
      previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity })
    },
    duration: 800,
    complete: function () {
      current_fs.hide()
      animating = false
    },
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  })
})

$('.submit').click(function (e) {
  e.preventDefault()
  if (validateStep(2)) {
    // alert('submitted!')
    // debugger;
    const form = getElement.form();
    // $('#spinner').show()
    form.submit();
  } else {
    // document.querySelector('#bottom').scrollIntoView({ behavior: "smooth" })
    window.scrollTo(0, document.body.scrollHeight);

  }

})

function fillHardCodedFields() {//For development purposes only
  getElement.password1().value = '1234567&'
  getElement.password2().value = '1234567&'
  getElement.email().value = 'sheff@gmail.com'
  getElement.name().value = 'Some name'

  getElement.about().innerHTML = 'some text...'
  getElement.phone().value = '05455555555'
  getElement.genre().value = 'Rock'
  getElement.talent().value = 'Musician'
  // getElement.city().value = 'Tel Aviv'

}

jQuery(document).ready(function ($) {
  const cities = getCities().map(city => city.eng_name.toLowerCase())
  // debugger;
  $("#city").autocomplete({
    source: cities
  });
  fillHardCodedFields()//For dev purposes only.
  $(window).keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  //open popup

  //Maps popup <a> element id to the actual popup container id.
  const popupTriggerMap = {
    'trigger-facebook': "facebook",
    'trigger-youtube': "youtube",
    'trigger-spotify': "spotify",
    'trigger-instagram': "instagram",
  }


  $('.cd-popup-trigger').on('click', function (event) {
    debugger;
    // e.stopPropagation()
    event.preventDefault();
    $(`#${popupTriggerMap[event.target.id]}`).addClass('is-visible');
  });


  $('.submit-link.action-button').on('click', function (event) {
    event.stopPropagation()
    validateModal(event);
    // validateStep(2)

  });



  $('.cd-popup').on('click', function (event) {

    if ($(event.target).is('.cd-popup-close, .submit-link') || $(event.target).is('.cd-popup')) {
      event.preventDefault();
      $(this).removeClass('is-visible');
    }
  });
  //close popup when clicking the esc keyboard button
  $(document).keyup(function (event) {
    if (event.which == '27') {
      $('.cd-popup').removeClass('is-visible');

    }
  });
});



//Returns an error string if an error is found, undefined otherwise.
function doesModalHaveErrors(id) {
  const value = getElement[id]().value;
  const error = hasErrors[id](value);
  if (error) {
    return error;
  }

}

function setModalError(id, error) {
  debugger;
  const modalErrorElement = getElement[`${id}-error`]();
  modalErrorElement.innerHTML = error;
  // const inputElement = getElement[id]();
  // const defaultColor = defaultBorderColor[id];
  // $(inputElement).css('borderColor',error ? 'red': defaultColor)
  // inputElement.style.borderColor = error ? 'red': defaultColor;
  // $(element).css('borderColor', hasError ? 'red' : defaultColor)
}

function validateModal() {


  // debugger;
  event.preventDefault();
  const parent = $(event.target).parents('.cd-popup')

  if (parent[0]) {
    var id = parent[0].id
    const error = doesModalHaveErrors(id)
    if (error) {
      setModalError(id, error)

      return;//Don't close the modal.
    } else {
      setModalError(id, "")//Clear the error.
    }
  }

  console.log(id)
  $('.cd-popup').removeClass('is-visible');

}

/*upload image*/

var getElement = {
  'general-error': function () { return document.querySelectorAll('.general-error') },
  'email': function () { return document.querySelector('#email') },
  'name': function () { return document.querySelector('#name') },
  'password': function () { return document.querySelectorAll('.password') },//This one returns two nodes.
  'password1': function () { return document.querySelector('#password1') },
  'password2': function () { return document.querySelector('#password2') },
  'phone': function () { return document.querySelector('#phone') },
  'genre': function () { return document.querySelector('#genre') },
  'city': function () { return document.querySelector('#city') },
  'date': function () { return document.querySelector('#datepicker') },
  'photo': function () { return document.querySelector('#photo') },
  'talent': function () { return document.querySelector('#talent') },
  'about': function () { return document.querySelector('#about') },
  'facebook': function () { return document.querySelector('#facebook-input') },
  'facebook-error': function () { return document.querySelector('#facebook-error') },
  'instagram': function () { return document.querySelector('#instagram-input') },
  'instagram-error': function () { return document.querySelector('#instagram-error') },
  'spotify': function () { return document.querySelector('#spotify-input') },
  'spotify-error': function () { return document.querySelector('#spotify-error') },
  'youtube': function () { return document.querySelector('#youtube-input') },
  'youtube-error': function () { return document.querySelector('#youtube-error') },
  'form': function () { return document.querySelector('#msform') },
  'delete-photo': function () { return document.querySelector('#delete-photo') },

}



let errorState = {
  phone: "",
  email: "",
  password: "",
  name: "",
  genre: "",
  city: "",
  date: "",
  talent: "",
  about: "",
  youtube: "",
  instagram: "",
  spotify: "",
  facebook: "",
  talent: "",
  socialMedia: "",
}


const defaultBorderColor = {
  youtube: 'black',
  spotify: 'black',
  instagram: 'black',
  facebook: 'black',
}


/**
 * Each function returns an error string if validation fails, or undefined if no errors found.
 */
var hasErrors = {
  email: function (value) {
    if (!validateEmail(value)) {
      return 'Please provide a valid email. '
    }
  },
  password: function (value1, value2) {
    if (!(CheckPassword(value1))) {
      return 'Password must be 8-15 characters long, and contain a symbol. '
    }
    if (value1 !== value2) {
      return 'Passwords do not match. '
    }

  },

  name: function (value) {

    if (value.length < 3) {
      return 'FullName must be at least 3 characters long. '
    }
  },
  city: function (value) {
    // debugger;
    if (value == 0) {
      return 'Must Select a city. '
    }
  },
  date: function (value) {

    if (!value) {
      return 'Must select a date. '
    }
  },
  phone: function (value) {

    if (!value || value.length < 9) {
      return 'Please provide a valid phone. '
    }
  },
  genre: function (value) {

    if (value == 0) {
      return 'Must select a genre. '
    }
  },

  talent: function (value) {

    if (value == 0) {
      return 'Must select a talent. '
    }
  },

  about: function (value) {

    if (!value) {
      return 'Please write something about you. '
    }
  },
  facebook: function (value) {
    if (!value.includes('facebook')) {
      return 'Facebook link is not valid. '
    }
  },
  spotify: function (value) {
    if (!value.includes('spotify')) {
      return 'Spotify link is not valid. '
    }
  },
  youtube: function (value) {
    if (!value.includes('youtube')) {
      return 'Youtube link is not valid. '
    }

  },
  instagram: function (value) {
    if (!value.includes('instagram')) {
      return 'Instagram link is not valid. '
    }
  },

  socialMedia() {
    let error = "";
    const facebook = getElement.facebook().value;
    const instagram = getElement.instagram().value;
    const spotify = getElement.spotify().value;
    const youtube = getElement.youtube().value;

    let counter = 0
    if (facebook) {
      const facebookError = hasErrors.facebook(facebook)
      if (facebookError) {
        error += facebookError
      } else {
        counter++
      }

    }
    if (instagram) {
      const instagramError = hasErrors.instagram(instagram)
      if (instagramError) {
        error += instagramError
      } else {
        counter++
      }

    }
    if (youtube) {
      const youtubeError = hasErrors.youtube(youtube)
      if (youtubeError) {
        error += youtubeError
      } else {
        counter++
      }

    }
    if (spotify) {
      const spotifyError = hasErrors.spotify(spotify)
      if (spotifyError) {
        error += spotifyError
      } else {
        counter++
      }
    }
    if (error) {
      return error;
    }
    if (counter == 0) {
      return 'Please provide at least one social media link. ';
    }
  }

}


function updateAllErrorFields() {
  // debugger;
  let arrayOfErrors = [];
  for (let prop in errorState) {
    if (prop === 'socialMedia') {
      // debugger;
    }
    const hasError = errorState[prop] ? true : false;
    // debugger;
    if (getElement[prop]) {
      const element = getElement[prop]();
      // element.style.borderColor = hasError ? 'red' : 'white';
      // $(element).css('borderColor', hasError ? 'red' : 'white')
      const defaultColor = defaultBorderColor[prop] ? defaultBorderColor[prop] : 'white'
      $(element).css('borderColor', hasError ? 'red' : defaultColor)
    }

    if (hasError) {
      arrayOfErrors.push(errorState[prop]);
    }
  }
  setGeneralError(arrayOfErrors);
}

/**
 * 
 * @param {number} step 
 * @return {boolean} 
 */
function validateStep(step) {
  // var errorString = "";
  // var errors = [];
  if (step == 1) {
    var email = getElement.email().value;
    var password1 = getElement.password1().value;
    var password2 = getElement.password2().value;
    var name = getElement.name().value;

    var errors = {
      email: hasErrors.email(email), password: hasErrors.password(password1, password2), name: hasErrors.name(name)
    }



    // errors = [hasErrors.email(email), hasErrors.password(password1, password2), hasErrors.name(name)]

  } else if (step == 2) {
    var phone = getElement.phone().value;
    var date = getElement.date().value;
    var genre = getElement.genre().value;
    var city = getElement.city().value;
    var talent = getElement.talent().value;
    var about = getElement.about().value;

    var errors = { socialMedia: hasErrors.socialMedia(), phone: hasErrors.phone(phone), date: hasErrors.date(date), genre: hasErrors.genre(genre), city: hasErrors.city(city), talent: hasErrors.talent(talent), about: hasErrors.about(about) }
  }

  errorState = {
    ...errorState,
    ...errors
  }

  let stepValid = true;
  for (let prop in errors) {//Check if this step has errors.
    if (errors[prop]) {
      stepValid = false;
      break;
    }
  }

  updateAllErrorFields()

  if (stepValid) {
    return true;
  }
  return false;

}


/**
 * 
 * @param {string[]} errors array of error strings 
 */
function setGeneralError(errors) {
  debugger;
  var element = getElement['general-error']();
  if (errors && errors.length) {
    let str = "<ul class='general-error-list'>"
    for (let error of errors) {
      str += `<li class="general-error-item">${error}</li>`
    }

    str+="</ul>"
   
    $(element).html(str).show();
  }else{
    $(element).html("").hide();
  }


}


function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}


function CheckPassword(inputtxt) {
  var paswd = /^(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,15}$/;
  if (inputtxt.match(paswd)) {
    // alert('Correct, try another...')
    return true;
  }
  else {
    // alert('Wrong...!')
    return false;
  }
}


function processFile(input) {
  // debugger;
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#blah')
        .attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }

  toggleDeleteFileButton(true)
}

function toggleDeleteFileButton(on){
  debugger;
  const element = getElement['delete-photo']();
  if(on){
    $(element).show()
  }else{
    $(element).hide()
  }
  
}

$('#delete-photo').click(()=>{
  const element = getElement.photo();
  element.value = "";
  toggleDeleteFileButton(false);

})



